package de.academy.blog.postslugs;

import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.regex.Pattern;

@Service
public class SlugService {

    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");
    private static final Pattern EDGESDHASHES = Pattern.compile("(^-|-$)");

    private SlugRepository slugRepository;

    public SlugService(SlugRepository slugRepository) {
        this.slugRepository = slugRepository;
    }

    public String titleToSlug(String title, String author) {
        String nowhitespace = WHITESPACE.matcher(title).replaceAll("-");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        slug = EDGESDHASHES.matcher(slug).replaceAll("");

        LocalDateTime now = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm-ss");

        String formatDateTime = now.format(formatter);
    
        Random random = new Random();
        
        String rnd = "" + random.nextInt(10000);
        

        slug = slug + "-" + formatDateTime + "-" + author + "-" + rnd;

        return slug;
    }

    public void saveSlug(Slug slug) {
        Slug tempSlug = new Slug();
        tempSlug.setPost(slug.getPost());
        
        tempSlug.setUrl(slug.getUrl());
        slugRepository.save(tempSlug);
    }

    public Slug findSlug(String slug) {
        return slugRepository.findByUrl(slug);
    }


}
