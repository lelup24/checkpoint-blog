package de.academy.blog.postslugs;

import de.academy.blog.comment.CommentDto;
import de.academy.blog.comment.CommentService;
import de.academy.blog.post.Post;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/")
public class SlugController {

    private SlugService slugService;
    private CommentService commentService;

    public SlugController(SlugService slugService, CommentService commentService) {
        this.slugService = slugService;
        this.commentService = commentService;
    }

    @GetMapping("/detail/{slug}")
    public String postDetail(Model model, @PathVariable String slug, Principal principal) {

        Slug tempSlug = slugService.findSlug(slug);

        Post tempPost = tempSlug.getPost();

        model.addAttribute("post", tempPost);
        model.addAttribute(new CommentDto());
        model.addAttribute("comments", commentService.getCommentsByPostId(tempPost.getId()));
        model.addAttribute("principal", principal);

        return "post-detail";
    }

}
