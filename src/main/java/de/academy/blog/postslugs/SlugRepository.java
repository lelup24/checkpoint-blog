package de.academy.blog.postslugs;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SlugRepository extends JpaRepository<Slug, Integer> {

    Slug findByUrl(String slug);

}
