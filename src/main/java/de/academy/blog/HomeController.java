package de.academy.blog;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import de.academy.blog.post.PostService;

@Controller
public class HomeController {
	
	private PostService postService;
	
	public HomeController(PostService postService) {
		this.postService = postService;
	}
	
	@GetMapping("/")
	public String home(Model model, Principal principal) {
		
		model.addAttribute("principal", principal);
		model.addAttribute("latestPosts", postService.getLatestPosts());
		
		return "index";
	}
	
	@GetMapping("/impressum")
	public String impressum(Model model, Principal principal) {
		model.addAttribute("principal", principal);
		return "impressum";
	}
	
}
