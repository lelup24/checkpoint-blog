package de.academy.blog.category;

import javax.validation.constraints.NotEmpty;

public class CategoryDto {

    private int id;
    @NotEmpty
    private String name;

    public CategoryDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
