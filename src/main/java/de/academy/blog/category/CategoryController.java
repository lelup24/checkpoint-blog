package de.academy.blog.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.security.Principal;


@Controller
public class CategoryController {

    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping("/categories")
    public String categories(Model model, Principal principal) {

        CategoryDto categoryDto = new CategoryDto();
        model.addAttribute("categoryDto", categoryDto);
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("principal", principal);

        return "categories";
    }

    @PostMapping("/categories/add")
    public String categoriesAdd(@Valid @ModelAttribute CategoryDto categoryDto, BindingResult bindingResult, Principal principal, Model model) {

        model.addAttribute("principal", principal);

        if (bindingResult.hasErrors()) {
            return "categories";
        }

        Category category = new Category(categoryDto.getName());
        categoryService.saveCategory(category);

        return "redirect:/categories";
    }

    @GetMapping("/categories/delete")
    public String categoriesAdd(@RequestParam int id) {

        categoryService.deleteCategory(categoryService.getCategoryById(id));

        return "redirect:/categories";
    }

}
