package de.academy.blog.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.academy.blog.file.FileEntity;
import de.academy.blog.file.FileRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserService implements IUserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private FileRepository fileRepository;
	
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	

	@Override
	@Transactional
	public User findByUserName(String userName) {
		// check the database if the user already exists
		return userRepository.findByUserName(userName);
	}
	
	@Override
	@Transactional
	public void save(UserRegistrationDto theUser) {
		User user = new User();
		 // assign user details to the user object
		user.setUserName(theUser.getUserName());
		user.setPassword(passwordEncoder.encode(theUser.getPassword()));
		user.setEmail(theUser.getEmail());
		user.setImage(fileRepository.getOne(3));
		
		// give user default role of "user"
		user.setRoles(Arrays.asList(roleRepository.findRoleByName("ROLE_USER")));

		 // save user in the database
		userRepository.save(user);
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userRepository.findByUserName(userName);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
				mapRolesToAuthorities(user.getRoles()));
	}

	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
		return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
	}

	@Override
	public List<User> getUsers() {
		return userRepository.findAll();
	}

	@Override
	public User getUser(String userName) {
		return userRepository.findByUserName(userName);
	}
	
	@Override
	public void addUser(User theUser) {
		
		userRepository.save(theUser);
		
	}

	@Override
	public User dtoToUser(UserRegistrationDto userDto) {
		User tempUser = userRepository.findByUserName(userDto.getUserName());
		
		// Erstellt eine Liste für die Rollen des Nutzers
		List<Role> roles = new LinkedList<Role>();
		
		// Nimmt die Ids und sucht die passenden Kategorien dazu, speichert diese Kategorien in der Liste
		for (String role : userDto.getRoles()) {
			Role tempRole = roleRepository.findById(Integer.parseInt(role)).get();
			roles.add(tempRole);
		}
		
		tempUser.setUserName(userDto.getUserName());
		tempUser.setEmail(userDto.getEmail());
		tempUser.setRoles(roles);
		
		return tempUser;
	}

	@Override
	public User dtoToUser(UserUpdateDto userDto) {
		User tempUser = userRepository.findByUserName(userDto.getUserName());
		
		// Erstellt eine Liste für die Rollen des Nutzers
		List<Role> roles = new LinkedList<Role>();
		
		// Nimmt die Ids und sucht die passenden Kategorien dazu, speichert diese Kategorien in der Liste
		for (String role : userDto.getRoles()) {
			Role tempRole = roleRepository.findById(Integer.parseInt(role)).get();
			roles.add(tempRole);
		}
		
		tempUser.setUserName(userDto.getUserName());
		tempUser.setEmail(userDto.getEmail());
		tempUser.setRoles(roles);
		return tempUser;
	}

	@Override
	public User getById(int id) {
		return userRepository.findById(id).get();
	}

	@Override
	public void setImage(User tempUser, FileEntity image) {
		User theUser = userRepository.getOne(tempUser.getId());
		theUser.setImage(image);
		userRepository.save(theUser);
		
	}
}
