package de.academy.blog.user;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import de.academy.blog.file.FileEntity;

public interface IUserService extends UserDetailsService {
	
	UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException;

	User findByUserName(String userName);

	void setImage(User tempUser, FileEntity image);

	User getById(int id);

	User dtoToUser(UserUpdateDto userDto);

	User dtoToUser(UserRegistrationDto userDto);

	void addUser(User theUser);

	User getUser(String userName);

	List<User> getUsers();

	void save(UserRegistrationDto theUser);
	
}
