package de.academy.blog.user;

import de.academy.blog.comment.Comment;
import de.academy.blog.file.FileEntity;
import de.academy.blog.post.Post;
import de.academy.blog.postlogs.PostLog;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String userName;
	
	private String password;
	
	private String email;

	@OneToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
	private FileEntity image;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "users_roles", 
	joinColumns = @JoinColumn(name = "user_id"), 
	inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Collection<Role> roles;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private List<Post> posts;
	
	@OneToMany(mappedBy = "user")
	private List<Comment> comments;
	

	@OneToMany(mappedBy = "user")
	private List<PostLog> postLogs;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public FileEntity getImage() {
		return image;
	}

	public void setImage(FileEntity image) {
		this.image = image;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<PostLog> getPostLogs() {
		return postLogs;
	}

	public void setPostLogs(List<PostLog> postLogs) {
		this.postLogs = postLogs;
	}


	
}
