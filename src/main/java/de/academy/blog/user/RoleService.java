package de.academy.blog.user;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class RoleService {
	
	private RoleRepository roleRepository;
	
	public RoleService(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}
	
	public List<Role> getRoles() {
		return roleRepository.findAll();
	}

	public void save(Role role) {
		roleRepository.save(role);
		
	}

}
