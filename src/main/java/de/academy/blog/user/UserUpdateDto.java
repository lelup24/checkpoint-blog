package de.academy.blog.user;

import javax.validation.constraints.NotEmpty;

import de.academy.blog.user.validation.FieldMatch;

@FieldMatch.List({
    @FieldMatch(first = "password", second = "matchingPassword", message = "Die Passwörter müssen übereinstimmen.")
})
public class UserUpdateDto {
	
	@NotEmpty(message = "Bitte Namen eingeben.")
	private String userName;
	
	@NotEmpty(message = "Bitte Passwort eingeben.")
	private String password;
	@NotEmpty(message = "Bitte Passwort bestätigen.")
	private String matchingPassword;
	
	private String email;
	
	private int image;
	
	private String [] roles;
	
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMatchingPassword() {
		return matchingPassword;
	}

	public void setMatchingPassword(String matchingPassword) {
		this.matchingPassword = matchingPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}
	
	

}
