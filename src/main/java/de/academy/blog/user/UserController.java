package de.academy.blog.user;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import de.academy.blog.file.FileEntity;
import de.academy.blog.post.Post;
import de.academy.blog.post.PostService;


@Controller
@RequestMapping("/user")
public class UserController {
	
	private UserService userService;
	private RoleService roleService;
	private PostService postService;
	
	@Autowired
	public UserController(UserService userService, RoleService roleService, PostService postService) {
		this.userService = userService;
		this.roleService = roleService;
		this.postService = postService;
	}
	
	@GetMapping("/all")
	public String getUsers(Model model, Principal principal) {
		
		List<User> userList = userService.getUsers();
		Role role = new Role();
		
		model.addAttribute("users", userList);
		model.addAttribute("role", role);
		model.addAttribute("principal", principal);
		
		
		return "user-list";
	}
	
	@PostMapping("add-role")
	public String addRole(@ModelAttribute Role role) {
		roleService.save(role);
		return "redirect:/user/all";
	}
	
	
	@GetMapping("/{userName}")
	public String getUser(@PathVariable String userName, Model model, Principal principal) {
		
		User tempUser = userService.getUser(userName);
		List<Role> tempRoles = roleService.getRoles();
		FileEntity tempFileEntity = new FileEntity();
		List<Post> thePosts = postService.getUserPosts(tempUser.getId());
		
		model.addAttribute("user", tempUser);
		model.addAttribute("posts", thePosts);
		model.addAttribute("roles", tempRoles);
		model.addAttribute("file", tempFileEntity);
		model.addAttribute("principal", principal);
		
		
		return "user-detail";
	}
	
	@PostMapping("/submit")
	public String updateUser(@ModelAttribute("user") UserUpdateDto userDto, @ModelAttribute("roles") Role roles) {
		
		
		
		// Gibt die Informationen an eine Hilfsmethode weiter, die aus dem DTO ein Post-Objekt macht
		User theUser = userService.dtoToUser(userDto);
	
		// Verweist auf das PostRepository
		userService.addUser(theUser);
		
		// redirect zu den aufgelisteten Posts
		return "redirect:/user/all";
	}
	

}
