package de.academy.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;


@EnableAutoConfiguration
@SpringBootApplication
public class BlogApplication  implements WebMvcConfigurer{

	public static void main(String[] args) {
		SpringApplication.run(BlogApplication.class, args);
	}
	
	// notwendig für th und sec in den Templates!!
	
	@Bean
	public SpringSecurityDialect securityDialect() {
	    return new SpringSecurityDialect();
	}
	
	
	

}
