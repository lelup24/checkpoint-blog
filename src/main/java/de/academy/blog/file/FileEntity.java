package de.academy.blog.file;

import de.academy.blog.post.Post;
import de.academy.blog.user.User;

import javax.persistence.*;

@Entity
public class FileEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String fileName;

	private String fileType;

	@OneToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.DETACH})
	private User user;

	@OneToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.DETACH})
	private Post post;

	@Lob
	private byte[] data;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public int getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}
}
