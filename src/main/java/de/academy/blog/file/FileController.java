package de.academy.blog.file;

import de.academy.blog.post.Post;
import de.academy.blog.post.PostService;
import de.academy.blog.user.User;
import de.academy.blog.user.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
public class FileController {
	
	private FileService fileService;
	private UserService userService;
    private PostService postService;


    public FileController(FileService fileService, UserService userService, PostService postService) {
		this.fileService = fileService;
		this.userService = userService;
        this.postService = postService;
	}

	
	@GetMapping("/uploadForm")
	public String uploadForm(Model model) throws IOException {
		
		FileEntity theFileEntity = new FileEntity();
		
		List<FileEntity> images = fileService.getAll();	
	    
		model.addAttribute("images", images);
		model.addAttribute("file", theFileEntity);
		
		
		return "upload";
	}
	
	@GetMapping(value = "/images/{id}")
	public ResponseEntity<byte []> images(@PathVariable int id, HttpServletResponse response) throws IOException {
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.parseMediaType(fileService.getFile(id).getFileType()));
		responseHeaders.setCacheControl("max-age=806400");
		
	    return new ResponseEntity<byte []>(fileService.getFile(id).getData(), responseHeaders, HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/user/images/{id}")
	public ResponseEntity<byte []> profiles (@PathVariable int id, HttpServletResponse response) throws IOException {
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.parseMediaType(fileService.getFile(id).getFileType()));
		responseHeaders.setCacheControl("max-age=806400");
		
	    return new ResponseEntity<byte []>(fileService.getFile(id).getData(), responseHeaders, HttpStatus.CREATED);
	}
	
	
	@PostMapping("/file/submit")
	public String upload(@RequestParam("file") MultipartFile file) {
		
		FileEntity currentFileEntity = new FileEntity();
		
		currentFileEntity.setFileName(file.getOriginalFilename());
		currentFileEntity.setFileType(file.getContentType());
		
		try {
			currentFileEntity.setData(file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		fileService.addFile(currentFileEntity);
		
		
		return "redirect:/uploadForm";
	}
	
	@PostMapping("/file/submitprofile/{id}")
    public String uploadprofile(@RequestParam("file") MultipartFile file, @PathVariable int id, RedirectAttributes redirectAttributes) {

        User tempUser = userService.getById(id);

        FileEntity oldFileEntity = tempUser.getImage();

        if (oldFileEntity != null) {
	        try {
	            oldFileEntity.setData(file.getBytes());
	            oldFileEntity.setFileName(file.getOriginalFilename());
	            oldFileEntity.setFileType(file.getContentType());
	            userService.setImage(tempUser, oldFileEntity);
	            fileService.save(oldFileEntity);

	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
        } else {
        
	        FileEntity currentFileEntity = new FileEntity();
	        
	        try {
				currentFileEntity.setData(file.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        currentFileEntity.setFileName(file.getOriginalFilename());
	        currentFileEntity.setFileType(file.getContentType());

	        fileService.save(currentFileEntity);
	
	        userService.setImage(tempUser, currentFileEntity);
        }
        
        
        redirectAttributes.addAttribute("userName", tempUser.getUserName());
       
        return "redirect:/user/{userName}";
    }


    @PostMapping("/file/submitpost/{id}")
    public String updatePostImg(@RequestParam("file") MultipartFile file, @PathVariable int id, RedirectAttributes redirectAttributes) {

        Post tempPost = postService.getPostById(id);
        FileEntity currentFileEntity = tempPost.getImage();
        
       

        currentFileEntity.setFileName(file.getOriginalFilename());
        currentFileEntity.setFileType(file.getContentType());
        currentFileEntity.setUser(tempPost.getUser());
        currentFileEntity.setPost(tempPost);

        try {
            currentFileEntity.setData(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        redirectAttributes.addAttribute("slug", tempPost.getSlug());


        fileService.addFile(currentFileEntity);


        return "redirect:/posts/detail/{slug}/edit";
    }

}
