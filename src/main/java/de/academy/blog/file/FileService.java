
package de.academy.blog.file;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService {
	
	private FileRepository fileRepository;
	
	public FileService(FileRepository fileRepository) {
		this.fileRepository = fileRepository;
	}
	
	public void addFile(FileEntity file) {
		fileRepository.save(file);
	}
	
	public FileEntity getFile(int id) {
		return fileRepository.getOne(id);
	}

	public List<FileEntity> getAll() {
		return fileRepository.findAll();
	}
	
	public FileEntity getByName(String name) {
		return fileRepository.getByFileName(name);
	}
	
	public Boolean exists(String fileName) {
		return fileRepository.existsByFileName(fileName);
	}

	public void delete(FileEntity oldFileEntity) {
		fileRepository.delete(oldFileEntity);

	}

	public void save(FileEntity oldFileEntity) {
		fileRepository.save(oldFileEntity);

	}

}
