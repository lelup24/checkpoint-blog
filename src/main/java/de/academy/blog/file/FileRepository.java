package de.academy.blog.file;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<FileEntity, Integer> {
	
	FileEntity getByFileName(String fileName);
	
	Boolean existsByFileName(String fileName);
}
