package de.academy.blog.post;

import de.academy.blog.category.Category;
import de.academy.blog.comment.Comment;
import de.academy.blog.file.FileEntity;
import de.academy.blog.postlogs.PostLog;
import de.academy.blog.postslugs.Slug;
import de.academy.blog.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Post {

    @Id
    @GeneratedValue
    private int id;
    private String title;

    @Lob
    private String text;
    private LocalDateTime postedAt;

    @ManyToOne()
    private User user;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "post", orphanRemoval = true)
    private List<Comment> comments = new ArrayList<>();

    @ManyToMany
    private List<Category> categories;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Slug> slugs;

    private String slug;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PostLog> postLog;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "post", orphanRemoval = true)
    private FileEntity image;


    public Post() {
    }

    public Post(User user, String title, String text, LocalDateTime postedAt) {
        this.user = user;
        this.title = title;
        this.text = text;
        this.postedAt = postedAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(LocalDateTime postedAt) {
        this.postedAt = postedAt;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }


    public FileEntity getImage() {
        return image;
    }

    public void setImage(FileEntity image) {
        this.image = image;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Slug> getSlugs() {
        return slugs;
    }

    public void setSlugs(List<Slug> slugs) {
        this.slugs = slugs;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }


	public List<PostLog> getPostLog() {
		return postLog;
	}

	public void setPostLog(List<PostLog> postLog) {
		this.postLog = postLog;
	}


}
