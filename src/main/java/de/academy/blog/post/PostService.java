package de.academy.blog.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class PostService {

    private PostRespository postRespository;

    @Autowired
    public PostService(PostRespository postRespository) {
        this.postRespository = postRespository;
    }

    public List<Post> getPosts() {

        return postRespository.findAllByOrderByPostedAtDesc();
    }


    public void savePost(Post post) {
        postRespository.save(post);
    }

    public void deletePost(Post post) {
        postRespository.delete(post);
    }

    public Post getPostById(int postId) {
        return postRespository.findById(postId).get();
    }

	public Post getPostBySlug(String slug) {
		return postRespository.findBySlug(slug);
	}
	
	public List<Post> getLatestPosts() {
		return postRespository.findFirst4ByOrderByPostedAtDesc();
	}


    public List<Post> getPostsByCategory(int id) {
        return postRespository.findAllByCategories_IdOrderByPostedAtDesc(id);
    }
    
    public List<Post> getUserPosts(int id) {
    	return postRespository.findByUserIdOrderByPostedAtDesc(id);
    }




}
