package de.academy.blog.post;


import de.academy.blog.category.Category;
import de.academy.blog.category.CategoryDto;
import de.academy.blog.category.CategoryService;
import de.academy.blog.comment.Comment;
import de.academy.blog.comment.CommentDto;
import de.academy.blog.comment.CommentService;
import de.academy.blog.file.FileEntity;
import de.academy.blog.file.FileService;
import de.academy.blog.postlogs.PostLog;
import de.academy.blog.postlogs.PostLogService;
import de.academy.blog.postslugs.Slug;
import de.academy.blog.postslugs.SlugService;
import de.academy.blog.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;


@Controller
@RequestMapping("/posts")
public class PostController {

    private PostService postService;
    private UserService userService;
    private CommentService commentService;
    private CategoryService categoryService;
    private FileService fileService;
    private SlugService slugService;
    private PostLogService postLogService;


    @Autowired
    public PostController(PostService postService, UserService userService, CommentService commentService, CategoryService categoryService, FileService fileService, SlugService slugService, PostLogService postLogService) {
        this.postService = postService;
        this.userService = userService;
        this.commentService = commentService;
        this.categoryService = categoryService;
        this.fileService = fileService;
        this.slugService = slugService;
        this.postLogService = postLogService;
    }


    @GetMapping("/newpost")
    public String post(Model model, Principal principal) {

        PostDto postDto = new PostDto();
        List<Category> categories = categoryService.getCategories();
        
        model.addAttribute("categories", categories);
        model.addAttribute("postDto", postDto);
        model.addAttribute("principal", principal);
        return "post-new";
    }

    @GetMapping("/all")
    public String allPosts(Model model, Principal principal) {

    	model.addAttribute("posts", postService.getPosts());

        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("categoryDto", new CategoryDto());
        model.addAttribute("principal", principal);

        return "allposts";
    }

    @PostMapping("/all/refresh")
    public String allPostsByCategory(@ModelAttribute CategoryDto categoryDto, RedirectAttributes redirectAttributes, Model model) {

        model.addAttribute(categoryDto);
        redirectAttributes.addAttribute("id", categoryDto.getId());


        if (categoryDto.getId() == 0) {
            return "redirect:/posts/all";
        }

        return "redirect:/posts/all/category";
    }

    @GetMapping("/all/category")
    public String allPostsFiltered(@RequestParam int id, @ModelAttribute CategoryDto categoryDto, Model model, Principal principal) {

        model.addAttribute("principal", principal);
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("posts", postService.getPostsByCategory(id));

        return "allposts";

    }


    @PostMapping("/save")
    public String post(@Valid @ModelAttribute PostDto postDto, BindingResult bindingResult, Principal principal, Model model) {

        model.addAttribute("principal", principal);

        if (bindingResult.hasErrors()) {
            return "post-new";
        }

        Post post = new Post(userService.findByUserName(principal.getName()),
                postDto.getTitle(), postDto.getText(), LocalDateTime.now());

        FileEntity defaultImage = new FileEntity();
        defaultImage.setData(fileService.getFile(1).getData());
        defaultImage.setFileName(fileService.getFile(1).getFileName());
        defaultImage.setFileType(fileService.getFile(1).getFileType());
        defaultImage.setUser(userService.findByUserName(principal.getName()));
        fileService.addFile(defaultImage);
        fileService.getFile(defaultImage.getId()).setPost(post);

        post.setImage(defaultImage);
        post.setCategories(postDto.getCategories());

        Slug slug = new Slug();
        
        
        post.setSlug(slugService.titleToSlug(postDto.getTitle(), post.getUser().getUserName()));
        slug.setPost(post);
      
        postService.savePost(post);
        slug.setUrl(post.getSlug());
        slugService.saveSlug(slug);

        return "redirect:/posts/all";
    }


    @PostMapping("/delete")
    public String deletePost(@RequestParam int postId) {

        postService.deletePost(postService.getPostById(postId));

        return "redirect:/posts/all";
    }

    @GetMapping("/detail/{slug}")
    public String postDetail(Model model, @PathVariable String slug, Principal principal) {

        Slug tempSlug = slugService.findSlug(slug);

        Post tempPost = tempSlug.getPost();

        model.addAttribute("post", tempPost);
        model.addAttribute(new CommentDto());
        model.addAttribute("comments", commentService.getCommentsByPostId(tempPost.getId()));
        model.addAttribute("principal", principal);

        return "post-detail";
    }

    @GetMapping("/detail/{slug}/postlog")
    public String showPostLog(Model model, @PathVariable String slug, Principal principal) {



        List<PostLog> postLogs = postService.getPostBySlug(slug).getPostLog();
        

        model.addAttribute("postLogs", postLogs);
        model.addAttribute("principal", principal);

   	
    	model.addAttribute("logs", postLogs);
    	
    	return "postlog";

    }

    @GetMapping("/detail/{slug}/edit")
    public String edit(@PathVariable String slug, Model model, Principal principal) {

        Post tempPost = postService.getPostBySlug(slug);
        
        List<Category> tempCategories = categoryService.getCategories();

        PostDto postDto = new PostDto();
        postDto.setTitle(tempPost.getTitle());
        postDto.setText(tempPost.getText());
        postDto.setId(tempPost.getId());
        postDto.setCategories(tempPost.getCategories());
        postDto.setImage(fileService.getFile(tempPost.getImage().getId()));
        model.addAttribute("postDto", postDto);
        model.addAttribute("slug", slug);
        model.addAttribute("principal", principal);
        model.addAttribute("categories", tempCategories);
        FileEntity tempFileEntity = new FileEntity();
        model.addAttribute(tempFileEntity);


        return "post-update";
    }

    @PostMapping("/detail/{slug}/update")
    public String update(@PathVariable String slug, @Valid @ModelAttribute PostDto postDto, BindingResult bindingResult, Principal principal) {

        if (bindingResult.hasErrors()) {
            return "post-update";
        }

        Post post = postService.getPostById(slugService.findSlug(slug).getPost().getId());
        
        String oldTitle = post.getTitle();
        String oldText = post.getText();
        String theSlugString = slugService.titleToSlug(postDto.getTitle(), principal.getName());
        post.setUser(userService.findByUserName(principal.getName()));
        post.setText(postDto.getText());
        post.setSlug(theSlugString);
        post.setTitle(postDto.getTitle());
        post.setCategories(postDto.getCategories());

    
        Slug theSlug = new Slug();
        theSlug.setPost(post);
        theSlug.setUrl(theSlugString);
        
        postLogService.writeLog(oldTitle, oldText, post, principal);
        

        
        postService.savePost(post);

        slugService.saveSlug(theSlug);
        postLogService.writeLog(oldTitle, oldText, post, principal);

        return "redirect:/posts/all";
    }


    @PostMapping("/detail/{slug}/savecomment")
    public String saveComment(@PathVariable String slug, @Valid @ModelAttribute CommentDto commentDto, BindingResult bindingResult, Principal principal) {
        
    	
    	
    	if (bindingResult.hasErrors()) {
    		
            return "redirect:/posts/detail/{slug}";
        }

        Comment comment = new Comment(commentDto.getText(), LocalDateTime.now(), postService.getPostBySlug(slug), userService.findByUserName(principal.getName()));

        commentService.saveComment(comment);

        return "redirect:/posts/detail/{slug}";
    }

    @PostMapping("/detail/{slug}/{commentId}/deletecomment")
    public String deleteComment(@PathVariable String slug, @PathVariable int commentId) {

        commentService.deleteComment(commentService.getCommentById(commentId).get());

        return "redirect:/posts/detail/{slug}";
    }


}
