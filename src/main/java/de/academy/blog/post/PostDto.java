package de.academy.blog.post;


import de.academy.blog.category.Category;
import de.academy.blog.file.FileEntity;

import javax.validation.constraints.NotEmpty;
import java.util.List;

public class PostDto {

    private int id;
    @NotEmpty
    private String title;
    @NotEmpty
    private String text;
    private List<Category> categories;
    private FileEntity image;


    public PostDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public FileEntity getImage() {
        return image;
    }

    public void setImage(FileEntity image) {
        this.image = image;
    }
}
