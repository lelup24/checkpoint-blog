package de.academy.blog.post;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRespository extends JpaRepository<Post, Integer> {

    List<Post> findAllByOrderByPostedAtDesc();



    List<Post> findAllByCategories_IdOrderByPostedAtDesc(int id);

	Post findBySlug(String slug);

	List<Post> findFirst4ByOrderByPostedAtDesc();
	
	List<Post> findByUserIdOrderByPostedAtDesc(int id);

}
