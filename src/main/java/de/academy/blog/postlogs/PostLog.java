package de.academy.blog.postlogs;

import de.academy.blog.post.Post;
import de.academy.blog.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class PostLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Lob
    private String old;

    @Lob
    private String updated;

    @ManyToOne
    private Post post;
    
    @ManyToOne
    private User user;

    private LocalDateTime changedAt;

    public String getOld() {
        return old;
    }

    public void setOld(String old) {
        this.old = old;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public int getId() {
        return id;
    }

    public LocalDateTime getChangedAt() {
        return changedAt;
    }

    public void setChangedAt(LocalDateTime changedAt) {
        this.changedAt = changedAt;
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
    
    

}
