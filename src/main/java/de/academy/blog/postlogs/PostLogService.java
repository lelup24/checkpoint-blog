package de.academy.blog.postlogs;

import de.academy.blog.post.Post;
import de.academy.blog.postslugs.SlugRepository;
import de.academy.blog.user.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;

@Service
public class PostLogService {

    private PostLogRepository postLogRepository;
    private SlugRepository slugRepository;
    private UserRepository userRepository;

    @Autowired
    public PostLogService(PostLogRepository postLogRepository, SlugRepository slugRepository, UserRepository userRepository) {
        this.postLogRepository = postLogRepository;
        this.slugRepository = slugRepository;
        this.userRepository = userRepository;
    }

    public void writeLog(String oldTitle, String oldText, Post updatedPost, Principal principal) {
        PostLog currentPostLog = new PostLog();

        String old = "";
        String updated = "";

        if (!oldTitle.equals(updatedPost.getTitle())) {
            old = ">>> Alter Titel >>> " + oldTitle;
            updated = ">>> Neuer Titel >>> " + updatedPost.getTitle();
        }  
        
        if (!oldText.equals(updatedPost.getText())) {
            old = old + " >>> Alter Inhalt >>> " + oldText;
            updated = updated + " >>> Neuer Inhalt >>>" + updatedPost.getText();
        }

        currentPostLog.setOld(old);
        currentPostLog.setUpdated(updated);
        currentPostLog.setChangedAt(LocalDateTime.now());
        currentPostLog.setPost(updatedPost);
        currentPostLog.setUser(userRepository.findByUserName(principal.getName()));

        postLogRepository.save(currentPostLog);

    }

    public PostLog getPostLog(String slug) {

        Post thePost = slugRepository.findByUrl(slug).getPost();

        PostLog currentPostLog = postLogRepository.findByPost(thePost);

        return currentPostLog;
    }

}
