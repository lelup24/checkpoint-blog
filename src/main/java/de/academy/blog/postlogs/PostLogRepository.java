package de.academy.blog.postlogs;

import de.academy.blog.post.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostLogRepository extends JpaRepository<PostLog, Integer> {

    PostLog findByPost(Post thePost);

}
