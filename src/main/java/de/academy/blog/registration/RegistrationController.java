package de.academy.blog.registration;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import de.academy.blog.user.User;
import de.academy.blog.user.UserRegistrationDto;
import de.academy.blog.user.UserService;


@Controller
@RequestMapping("/register")
public class RegistrationController {
	
	@Autowired
	private UserService userService;


	@GetMapping("/registrationform")
	public String registration(Model model) {
		UserRegistrationDto tempUserRegistrationDto = new UserRegistrationDto();
		
		model.addAttribute("userRegistrationDto", tempUserRegistrationDto);
		
		return "register";
	}
	
	@PostMapping("/submit")
	public String processRegistrationForm(
				@Valid @ModelAttribute("userRegistrationDto") UserRegistrationDto theUser, 
				BindingResult theBindingResult, 
				Model theModel) {
		
		String userName = theUser.getUserName();
		
		// form validation
		 if (theBindingResult.hasErrors()){
			 return "register";
	        }

		// check the database if user already exists
        User existing = userService.findByUserName(userName);
        if (existing != null){
        	theModel.addAttribute("userRegistrationDto", new UserRegistrationDto());
			theModel.addAttribute("registrationError", "Der Benutzername ist bereits vergeben.");

        	return "register";
        }
     // create user account        						
        userService.save(theUser);
        
        
        return "registration-confirmation";		

	}
}
