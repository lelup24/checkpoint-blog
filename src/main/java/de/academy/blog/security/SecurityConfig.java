package de.academy.blog.security;

import de.academy.blog.user.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	
	@Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		
		// Datenbankzugriff
		auth.authenticationProvider(authenticationProvider());
    }
	

	
 
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
    	http.authorizeRequests()
			.antMatchers("/").permitAll()
			.antMatchers("/uploadForm").hasRole("ADMIN")
			.antMatchers("/file/*").hasRole("ADMIN")
			.antMatchers("/user").hasRole("ADMIN")
			.antMatchers("/categories").hasRole("ADMIN")
			.antMatchers("/categories/*").hasRole("ADMIN")
			.antMatchers("/user/{username}").permitAll()
			.antMatchers("/posts/newpost").hasRole("ADMIN")
			.antMatchers("/posts/save").hasRole("ADMIN")
			.antMatchers("/posts/delete").hasRole("ADMIN")
			.antMatchers("/posts/detail/{slug}/edit").hasRole("ADMIN")
			.antMatchers("/posts/detail/{slug}/update").hasRole("ADMIN")
			.antMatchers("/posts/detail/{slug}/savecomment").hasRole("ADMIN")
			.antMatchers("/posts/detail/{slug}/deletecomment").hasRole("ADMIN")
			.antMatchers("/posts/*").permitAll()
		.and()
		.formLogin()
			.loginPage("/login")
			.loginProcessingUrl("/athenticateTheUser")
			.permitAll()
		.and()
			.logout()
				.invalidateHttpSession(true)
				.clearAuthentication(true)
			.logoutSuccessUrl("/")
				.permitAll()
		.and()
			.exceptionHandling().accessDeniedPage("/access-denied")
		.and()
             .rememberMe()
                 .rememberMeCookieName("remember-me-cookie-name")
                 .tokenValiditySeconds(24 * 60 * 60)
                 .userDetailsService(userService);

    }
     
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
		auth.setUserDetailsService(userService); // setzt den DetailService, der im UserService implementiert ist
		auth.setPasswordEncoder(passwordEncoder()); // setzt den Encoder (BCrypt)
		return auth;
	}

}
