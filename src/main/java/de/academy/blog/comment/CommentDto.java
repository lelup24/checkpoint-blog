package de.academy.blog.comment;

import javax.validation.constraints.NotEmpty;
import java.time.Instant;

public class CommentDto {

    @NotEmpty
    private String text;
    private Instant postedAt;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }
}
