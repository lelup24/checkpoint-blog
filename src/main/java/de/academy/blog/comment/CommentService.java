package de.academy.blog.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    private CommentRepository commentRepository;

    public CommentService() {
    }

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }


    public void saveComment(Comment comment) {
        commentRepository.save(comment);
    }

    public void deleteComment(Comment comment) {
        commentRepository.delete(comment);
    }

    public Optional<Comment> getCommentById(int commentId) {
        return commentRepository.findById(commentId);
    }

    public List<Comment> getCommentsByPostId(int postId) {

        return commentRepository.findCommentsByPost_IdOrderByPostedAtAsc(postId);
    }
}
